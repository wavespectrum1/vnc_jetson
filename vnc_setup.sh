sudo apt update
sudo apt install vino
# Inform thae system which graphic display to interact with
export DISPLAY=:0

mkdir -p ~/.config/autostart

# Enable the VNC server
gsettings set org.gnome.Vino enabled true
gsettings set org.gnome.Vino prompt-enabled false
gsettings set org.gnome.Vino require-encryption false

cp /usr/share/applications/vino-server.desktop ~/.config/autostart/.
sudo cp org.gnome.Vino.gschema.xml /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml
sudo glib-compile-schemas /usr/share/glib-2.0/schemas

cd /usr/lib/systemd/user/graphical-session.target.wants
sudo ln -sf ../vino-server.service ./.

# Replace thepassword with your desired password
gsettings set org.gnome.Vino authentication-methods "['vnc']"
gsettings set org.gnome.Vino vnc-password $(echo -n 'pass'|base64)
gsettings set org.gnome.Vino use-alternative-port true
sudo reboot

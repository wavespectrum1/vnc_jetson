# vnc_jetson

## clone the repo
```
git clone https://gitlab.com/wavespectrum1/vnc_jetson.git
```

## run vnc_setup
```
cd vnc_jetson
sh vnc_jetson.sh
```

## connect to jetson using vnc viewer
<jetson_ip_address>:5901
